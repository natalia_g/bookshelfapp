package com.example.bookshelf;

import io.restassured.RestAssured;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.with;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.startsWith;

class BookshelfAppTest {
    private static final String BOOK_1 = "{\n" +
            "  \"id\": 13,\n" +
            "  \"title\": \"Harry Potter i Zakon Feniksa\",\n" +
            "  \"author\": \"J.K\",\n" +
            "  \"pageSum\": 480,\n" +
            "  \"yearOfPublished\": 2005,\n" +
            "  \"publishingHouse\": \"publishing company ###\"\n" +
            "}";
    private static final String BOOK_2 = "{\n" +
            "  \"id\": 13,\n" +
            "  \"title\": \"Harry Potter i Zakon Feniksa\",\n" +
            "  \"author\": \"J.K\",\n" +
            "  \"pageSum\": \"480 pages\",\n" +
            "  \"yearOfPublished\": 2005,\n" +
            "  \"publishingHouse\": \"publishing company ###\"\n" +
            "}";

    private static final int APP_PORT = 8090;
    private BookshelfApp bookshelfApp;

    @BeforeAll
    public static void beforeAll(){
        RestAssured.port = APP_PORT;
    }

    @BeforeEach
    public void beforeEach() throws Exception {
        bookshelfApp = new BookshelfApp(APP_PORT);
    }

    @AfterEach
    public void afterEach(){
        bookshelfApp.stop();
    }

    @Test
    public void addMethod_correctBody_shouldReturnStatus200() throws Exception {
       // body odpowiedzi musi być identyczne z tym, co wpisane w "equalsTo()"
       // with().body(BOOK_1).when().post("/book/add").then().statusCode(200).body(equalTo("success"));
        with().body(BOOK_1).when().post("/book/add").then().statusCode(200).body(startsWith("Book has been successfully added, id="));
    }

    @Test
    public void addMethod_fieldTypeMismatch_shouldReturnStatus500() throws Exception {
        with().body(BOOK_2).when().post("/book/add").then().statusCode(500);
    }

    @Test
    public void addMethod_unexpectedField_shouldReturnStatus500() throws Exception {
        with().body("{\"numberOfChapters\":10").when().post("/book/add").then().statusCode(500);
    }

    private long addBookAndGetId(String json){
        String responseText = with().body(json).when().post("/book/add").then().statusCode(200).extract().body().asString();
        String idString = responseText.substring(responseText.indexOf("=") + 1);
        return Long.parseLong(idString);
    }

    @Test
    public void getMethod_correctBookIdParam_shouldReturnStatus200(){
        long bookId1 = addBookAndGetId(BOOK_1);
        with().param("bookId", bookId1).when().get("/book/get").then().statusCode(200)
                .body("id", equalTo(bookId1))
                .body("title", equalTo("Book fake @@"))
                .body("author", equalTo("Author fake"))
                .body("pagesSum", equalTo(133))
                .body("yearOfPublished", equalTo(1947))
                .body("publishingHouse", equalTo("Wydawnictwo fake"));
    }

}