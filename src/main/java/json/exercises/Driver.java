package json.exercises;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

public class Driver {

    public static void main(String[] args) {

        ObjectMapper mapper = new ObjectMapper();

        try{
            Student student1 = mapper.readValue(new File("data/sample-lite.json"), Student.class);
            Student student2 = mapper.readValue(new File("data/sample-full.json"), Student.class);
            System.out.println("First student's name: "+ student1.getFirstName());
            System.out.println("First student's last name: "+ student1.getLastName());
            System.out.println("Second student's name: "+ student2.getFirstName());
            System.out.println("Second student's last name: "+ student2.getLastName());
            System.out.println("Second student's languages: "+ Arrays.toString(student2.getLanguages()));
            System.out.println("Second student's address: "+ student2.getAddress());

        } catch (IOException e) {
            e.printStackTrace();
        }





    }
}
