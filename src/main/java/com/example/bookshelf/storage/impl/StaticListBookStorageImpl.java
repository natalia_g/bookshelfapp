package com.example.bookshelf.storage.impl;

import com.example.bookshelf.storage.BookStorage;
import com.example.bookshelf.type.Book;

import java.util.ArrayList;
import java.util.List;

public class StaticListBookStorageImpl implements BookStorage {
    private static List<Book> bookStorage = new ArrayList<Book>();

    public StaticListBookStorageImpl() {
        bookStorage.add(new Book(10, "Harry Potter i Czara Ognia","J.K", 500,2002,"publishing company ###"));
        bookStorage.add(new Book(11, "Harry Potter i Komnata Tajemnic","J.K", 400,2004,"publishing company ###"));
    }

    public Book getBook(long id) {
        for (Book book : bookStorage){
            if(book.getId() == id){
                return book;
            }
        }
        return null;
    }

    public List<Book> getAllBooks() {
       // System.out.println();
        return bookStorage;
    }

    public void addBook(Book book) {
        bookStorage.add(book);
    }

    public void removeBook(long bookToRemoveId) {
        for (Book book : bookStorage){
            if(book.getId() == bookToRemoveId){
                bookStorage.remove(book);
                // jak już znaleźliśmy książkę, to nie iterujemy dalej po liscie żeby nie tracić czasu
                break;
            }
        }
    }

    public void editBook(long id, Book newBook){
        // zrobić
    }


}
